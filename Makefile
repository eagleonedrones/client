#!make
include .env
export $(shell sed 's/=.*//' .env)


.PHONY: build run test clean

build:
	docker-compose build \
	 --build-arg GIT_PRIVATE_KEY="$(shell cat ~/.ssh/id_rsa | perl -pe 's/\n/__x__/g')"

keys/hub_rsa.pub:
	curl http://${HUB_HOST}:${APP_PORT}/public_key > keys/hub_rsa.pub

run: keys/hub_rsa.pub
	docker-compose run cli ${CMD}

test:
	docker-compose -f docker-compose.test.yaml build \
	 --build-arg GIT_PRIVATE_KEY="$(shell cat ~/.ssh/id_rsa | perl -pe 's/\n/__x__/g')"
	trap 'docker-compose down' EXIT; docker-compose -f docker-compose.test.yaml run client test

clean:
	docker-compose down --remove-orphans -v
