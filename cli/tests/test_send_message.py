import asyncio

from hub.tests.handlers.ws.test_delivery import TestCaseWithSomeDevicesAndDeliverancesDefined

from cli.commands.send_message import handler
from utils.aiotools import run_in_sync, sleep_until
from utils.crypting import get_rsa_keys
from utils.device_id import get_device_id_from_key_pub
from utils.testing import with_setup_and_finish


class SendMessageFromCliTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def skip_test_debug_message_to_single_device(self):
        await sleep_until(lambda: self.drone_co_1.websocket)
        await handler('debug-alert', f'cli_alias=cli', f'receiver_id={self.drone_co_1.device_id}',
                      '{}', _async=True)()

        await asyncio.sleep(.1)
        self.assertEqual(1, len(self.drone_co_1.received_messages))

    @run_in_sync
    @with_setup_and_finish
    async def skip_test_debug_messages_with_diffent_cli(self):
        await sleep_until(lambda: self.drone_co_1.websocket)
        await handler('debug-alert', f'cli_alias=cli', f'receiver_id={self.drone_co_1.device_id}',
                      '{}', _async=True)()
        await handler('debug-alert', f'cli_alias=cli2', f'receiver_id={self.drone_co_1.device_id}',
                      '{}', _async=True)()
        await handler('debug-alert', f'cli_alias=cli2', f'receiver_id={self.drone_co_1.device_id}',
                      '{}', _async=True)()

        cli_device_id = get_device_id_from_key_pub(get_rsa_keys(pair_name='cli')[0])
        cli2_device_id = get_device_id_from_key_pub(get_rsa_keys(pair_name='cli2')[0])

        await asyncio.sleep(.1)
        self.assertSetEqual({cli_device_id, cli2_device_id, cli2_device_id}, {m.sender_id for m in self.drone_co_1.received_messages})
