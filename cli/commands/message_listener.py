import asyncio
import os
import sys

from rsa import PublicKey
# noinspection PyUnresolvedReferences
from sh import sh

from client.client import HubClient
from utils.aiotools import run_in_sync, sleep_until
from utils.crypting import get_rsa_keys

HUB_HOST = os.environ.get('HUB_HOST', '37.157.193.224')
HUB_PORT = os.environ.get('HUB_PORT', '80')
RSA_KEYS_PATH = os.environ.get('RSA_KEYS_PATH', 'keys')

HUB_WS_URL = f'ws://{HUB_HOST}:{HUB_PORT}/ws'
# HUB_WS_URL = f'ws://{TEST_HUB_HOST}:{TEST_HUB_PORT}/ws'


async def _run_listener(cli_alias, script_to_call):
    try:
        with open(os.path.join(RSA_KEYS_PATH, 'hub_rsa.pub'), 'rb') as f:
            hub_key_pub = PublicKey.load_pkcs1(f.read(), format='PEM')

    except (FileNotFoundError, ValueError):
        sys.stderr.write('ERROR: HUB\'s public key not provided or invalid')
        return

    print('creating / loading RSA keys...')
    key_pub, key = get_rsa_keys(pair_name=cli_alias)
    client = HubClient(HUB_WS_URL, key_pub, key, hub_key_pub, auto_reconnect=True)

    async def read_messages_and_evaluate():
        await sleep_until(lambda: client.websocket)
        print(f'listening... (device_id: {client.device_id})')

        async for message in client:
            # evaluate
            if script_to_call:
                sh(script_to_call, str(message))

            else:
                print(message)

    print('connecting...')
    await asyncio.gather(client.connect(),
                         read_messages_and_evaluate())


def handler(cli_alias, script_to_call=None, _async=False):
    """
    Starts a daemon that will connect to HUB and every received message
    evaluates using provided filename. When no filename provided,
    received messages are printed.

    When no executable provided, incoming messages are printed to stdout.

    Require this file:
        - <settings.RSA_KEYS_PATH>/hub_rsa.pub  (HUB's public key)

    Example calls:
        ./hub-cli.py message-listener test_device_2 ./store_messages.sh
        ./hub-cli.py message-listener test_device_3
    """

    # allow async for testing
    cb = lambda: _run_listener(cli_alias, script_to_call)
    if _async:
        return cb
    return run_in_sync(cb)()


if __name__ == '__main__':
    handler('device_1')
