from .generate_keys import handler as generate_keys
from .message_listener import handler as message_listener
from .send_message import handler as send_message


def _help_handler():
    print('list of valid commands:')

    for command_name, handler in command_map.items():
        print(command_name)
        print(handler.__doc__ or '\n    -')
        print('')
        print('')


command_map = {
    'help': _help_handler,
    'generate-keys': generate_keys,
    'message-listener': message_listener,
    'send-message': send_message,
}

