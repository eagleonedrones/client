import asyncio
import json
import os
import sys

import time
from rsa import PublicKey

from client.client import HubClient
from utils.aiotools import run_in_sync, sleep_until
from utils.crypting import get_rsa_keys

HUB_HOST = os.environ.get('HUB_HOST', '37.157.192.224')
HUB_PORT = os.environ.get('HUB_PORT', '80')
RSA_KEYS_PATH = os.environ.get('RSA_KEYS_PATH', 'keys')

HUB_WS_URL = f'ws://{HUB_HOST}:{HUB_PORT}/ws'
# HUB_WS_URL = f'ws://{TEST_HUB_HOST}:{TEST_HUB_PORT}/ws'


async def _send_message(cli_alias, delivery_confirmation, interval, **kwargs):
    try:
        with open(os.path.join(RSA_KEYS_PATH, 'hub_rsa.pub'), 'rb') as f:
            hub_key_pub = PublicKey.load_pkcs1(f.read(), format='PEM')

    except (FileNotFoundError, ValueError):
        sys.stderr.write('ERROR: HUB\'s public key not provided or invalid')
        return

    print('creating / loading RSA keys...')
    key_pub, key = get_rsa_keys(pair_name=cli_alias)
    client = HubClient(HUB_WS_URL, key_pub, key, hub_key_pub, auto_reconnect=bool(interval))

    async def send_and_disconnect():
        async def _send():
            print('sending...')
            if client.websocket:
                t0 = time.time()
                await client.send(**kwargs, _wait_for_confirmation=delivery_confirmation)

                print(f'message has been successfully sent in {str(time.time() - t0)[:4]}s using   sender_id = {client.device_id}')
                print(str(client.sent_messages[0]))

            else:
                print('message not sent, client not connected')

        while interval:
            await _send()
            await asyncio.sleep(interval)

        else:
            await sleep_until(lambda: client.websocket)
            await _send()

        await client.disconnect()

    print('connecting...')
    await asyncio.gather(client.connect(), send_and_disconnect())


def handler(message_type, *args, _async=False):
    """
    Sends a single message to HUB, waits for confirmation and shuts.

    Require this file:
        - <settings.RSA_KEYS_PATH>/hub_rsa.pub  (HUB's public key)

    Optional parameters:
        - receiver_id=<receiver_id>
        - location=<location_id>

        - cli_alias=<cli_alias_name>
        - delivery_confirmation=<true|false>
        - interval=<pause between two messages in seconds>

    Example calls:
        ./hub-cli.py send-message info cli_alias=<test_device_1> location=co {"status": "alive"}
        ./hub-cli.py send-message info receiver_id=<test_device_2> {"status": "alive"}
    """

    params = list(args)
    data = params.pop()
    data = json.loads(data)

    params = [item.split('=') for item in params]
    params = {item[0]: item[1] for item in params}

    interval = float(params.pop('interval', 0))
    cli_alias = params.pop('cli_alias', None)
    delivery_confirmation = params.pop('delivery_confirmation', 'true')
    delivery_confirmation = delivery_confirmation.lower() in {'1', 't', 'true'}

    params = {key: params.get(key, None) for key in {'location', 'receiver_id'}}

    # allow async for testing
    cb = lambda: _send_message(cli_alias, delivery_confirmation, interval, message_type=message_type, data=data, **params)
    if _async:
        return cb
    return run_in_sync(cb)()
