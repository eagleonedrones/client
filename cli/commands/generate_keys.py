import os

from utils.crypting import get_rsa_keys

RSA_KEYS_PATH = os.environ.get('RSA_KEYS_PATH', 'keys')


def handler(cli_name):
    """
    Generates pair of RSA keys. "id_rsa.pub" for Public Key,"id_rsa" for Private Key
    """
    get_rsa_keys(pair_name=cli_name)
    return f'"{cli_name}_rsa" and "{cli_name}_rsa.pub" files were stored to { RSA_KEYS_PATH }'
