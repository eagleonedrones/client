from .commands import command_map


def run_command(command_name, args):
    if command_name not in command_map:
        print(f'Command "{ command_name }" not Found')
        return

    return command_map[command_name](*args)
