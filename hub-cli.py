#!.venv/bin/python
import sys

from cli import run_command

if __name__ == '__main__':
    _, command_name, *args = sys.argv

    # strip empty args (created by `docker-entrypoint.sh`)
    args = [arg for arg in args if arg]

    run_command(command_name, args)
