#!/bin/bash

set -e

# Commands available using `docker-compose run client [COMMAND]`
case "$1" in
    python)
        /venv/bin/python
    ;;
    test)
        /venv/bin/python -m unittest
    ;;
    *)
        echo "Running hub-cli.py command..."
        /venv/bin/python hub-cli.py "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
    ;;
esac
