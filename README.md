# Client

Modul pro komunikaci s HUBem.

## Testování

Závislostmi jsou pouze `Docker` a `docker-compose`. Pak již lze volat přímo `make && make test`.

## Cli

Poslání zprávy:

```sh

make CMD="\
	send-message debug-alert \
	cli_alias=device_2 receiver_id=\"+kzmjm\" \
	\"{\\\"text\\\": \\\"Dlouhá zpráva!\\\"}\" \
	" run
```

ev.

```sh
docker-compose run cli \
	send-message debug-alert \
	cli_alias=device_2 receiver_id='+kzmjm' \
	'{"text": "Dlouhá zpráva!"}'
```


Příjem zpráv:

```sh
make CMD="message-listener device_1" run
```

ev.

```sh
docker-compose run cli message-listener device_1
```