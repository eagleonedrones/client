import json
from typing import List

import time


class DeliveranceInput:
    def __init__(self, message_type: str, device_types: List[str]):
        self.message_type = message_type
        self.device_types = device_types


class DeviceInput:
    def __init__(self, device_id: int, device_type: str, locations: List[str]):
        self.id = device_id
        self.device_type = device_type
        self.locations = locations


class GeneralDataInput:
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

        self.plain = kwargs

    def __str__(self):
        return json.dumps(self.plain)


class MessageToHub:
    def __init__(self, message_type: str, data: dict, location: str=None,
                 receiver_id: str=None, emitted: float=None):

        self.emitted = emitted or time.time()
        self.message_type = message_type
        self.data = GeneralDataInput(**data)
        self.location = location
        self.receiver_id = receiver_id

        if self.location and self.receiver_id:
            raise ValueError('`location` and `receiver_id` can\'t be set together')

    @property
    def plain(self):
        message = {
            'emitted': self.emitted,
            'type': self.message_type,
            'data': self.data.plain,
        }
        if self.location:
            message['location'] = self.location
        if self.receiver_id:
            message['receiver_id'] = self.receiver_id
        return message

    def __str__(self):
        return json.dumps(self.plain)

    @classmethod
    def from_plain(cls, plain):
        return cls(message_type=plain['type'],
                   data=plain['data'],
                   emitted=plain.get('emitted', None),
                   location=plain.get('location', None),
                   receiver_id=plain.get('receiver_id', None))

    @classmethod
    def load(cls, json_string):
        return cls.from_plain(json.loads(json_string))


class MessageFromHub:
    def __init__(self, message_type: str, data: dict, emitted: float,
                 sender_id: str, stored: float=None):

        self.emitted = emitted
        self.stored = stored or time.time()
        self.message_type = message_type
        self.data = GeneralDataInput(**data)
        self.sender_id = sender_id

    @property
    def plain(self):
        return {
            'emitted': self.emitted,
            'stored': self.stored,
            'type': self.message_type,
            'data': self.data.plain,
            'sender_id': self.sender_id,
        }

    def __str__(self):
        return json.dumps(self.plain)

    @classmethod
    def from_plain(cls, plain):
        return cls(message_type=plain['type'],
                   data=plain['data'],
                   emitted=plain['emitted'],
                   sender_id=plain['sender_id'],
                   stored=plain.get('stored', None))

    @classmethod
    def load(cls, json_string):
        return cls.from_plain(json.loads(json_string))
