

MESSAGE_PING = 'ping'
MESSAGE_PING_DATA = {'message': 'Hey!'}

MESSAGE_PONG = 'pong'
MESSAGE_PONG_DATA = {'message': 'Hi!'}

MESSAGE_CONFIRMATION = 'delivery-confirmation'

MESSAGE_UNDEFINED_CLIENT = 'authorization-required'
MESSAGE_UNDEFINED_CLIENT_DATA = {}
