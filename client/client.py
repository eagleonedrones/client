import asyncio
import os
from typing import List

import websockets

from client.connection import HubConnection
from client.handshake import client_side_handshake
from client.input_formats import MessageToHub, MessageFromHub
from client.responses import MESSAGE_CONFIRMATION
from utils.aiotools import sleep_until
from utils.device_id import get_device_id_from_key_pub

PING_INTERVAL = int(os.environ.get('PING_INTERVAL', 1))
PING_TIMEOUT = int(os.environ.get('PING_TIMEOUT', PING_INTERVAL * 3))
CLOSE_TIMEOUT = int(os.environ.get('CLOSE_TIMEOUT', PING_TIMEOUT / 2))
RECONNECT_INTERVAL = int(os.environ.get('RECONNECT_INTERVAL', 1))


class HubClient:
    def __init__(self, ws_url, key_pub, key, hub_key_pub, auto_reconnect=False):
        self.websocket = None
        self.ws_url = ws_url
        self.key_pub, self.key = key_pub, key
        self.hub_key_pub = hub_key_pub
        self.device_id = get_device_id_from_key_pub(self.key_pub)

        self.auto_reconnect = auto_reconnect

        self.received_messages: List[MessageFromHub] = []
        self.sent_messages: List[MessageToHub] = []

        self._confirmation_futures = {}
        self._new_message_futures = []

        self._last_msg_index = 0
        self._is_closing = False

    def __aiter__(self):
        return self

    async def __anext__(self) -> MessageFromHub:
        last_received_message = await self.get(self._last_msg_index)
        self._last_msg_index = last_received_message.emitted
        return last_received_message

    @property
    def connection(self):
        if self.websocket:
            return HubConnection(self.key, self.hub_key_pub, self.websocket)
        else:
            raise ValueError

    async def connect(self, silent_disconnect=False):
        while self.auto_reconnect:
            try:
                await self._connect(silent_disconnect=False)
            except websockets.ConnectionClosed:
                print('disconnected from HUB')
            except (ConnectionRefusedError,
                    websockets.InvalidMessage,
                    websockets.InvalidStatusCode):
                print(f'unable to connect to HUB, trying again in to in { RECONNECT_INTERVAL } s')
                await asyncio.sleep(RECONNECT_INTERVAL)

        else:
            await self._connect(silent_disconnect=silent_disconnect)

    async def _connect(self, silent_disconnect=False):
        async with websockets.connect(self.ws_url,
                                      # ping_interval=PING_INTERVAL,
                                      # ping_timeout=PING_TIMEOUT,
                                      # close_timeout=CLOSE_TIMEOUT,
                                      ) as websocket:
            print('connected to HUB')
            try:
                # assign websocket after handshake (to see whether handshake has been already done, etc.)
                await client_side_handshake(self.key_pub, HubConnection(self.key, self.hub_key_pub, websocket))
                self.websocket = websocket

                await self._send_unsent_messages()

                # start receiving messages
                async for message in self.connection:
                    self.received_messages.append(message)

                    # fire all waiting futures
                    while len(self._new_message_futures):
                        new_message_future = self._new_message_futures.pop(0)
                        new_message_future.set_result(message)

                    # check for delivery confirmations
                    if MESSAGE_CONFIRMATION == message.message_type:
                        confirmation_future = self._confirmation_futures.get(message.data.emitted, None)
                        if confirmation_future and not confirmation_future.done():
                            confirmation_future.set_result(True)

            except websockets.ConnectionClosed as error:
                raise_error = not silent_disconnect and not self._is_closing
                await self.disconnect()
                if raise_error:
                    raise error

    async def get(self, last_message_emitted=0):
        for message in self.received_messages:
            if last_message_emitted < message.emitted:
                return message

        new_message_future = asyncio.Future()
        self._new_message_futures.append(new_message_future)
        return await new_message_future

    async def send(self, message_type, data, location=None, receiver_id=None, emitted=None,
                   _wait_for_confirmation=False):

        message = MessageToHub(message_type=message_type,
                               data=data,
                               location=location,
                               receiver_id=receiver_id,
                               emitted=emitted)

        # wait until connected
        await sleep_until(lambda: self.websocket, timeout=20)

        await self.connection.send(message)
        self.sent_messages.append(message)
        if _wait_for_confirmation:
            await self.delivery_confirmation(message)

    def delivery_confirmation(self, message: MessageToHub):
        confirmation_future = asyncio.Future()
        self._confirmation_futures[message.emitted] = confirmation_future
        return confirmation_future

    async def _send_unsent_messages(self):
        for message in self.sent_messages:
            await self.connection.send(message)

    async def disconnect(self):
        self._is_closing = True
        if self.websocket:
            websocket, self.websocket = self.websocket, None
            await websocket.close()
