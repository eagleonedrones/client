from rsa import PrivateKey, PublicKey
from websockets import WebSocketCommonProtocol

from client.errors import HubConnectionError
from client.input_formats import MessageToHub, MessageFromHub
from utils.crypting import encrypt, decrypt
from utils.device_id import get_device_id_from_key_pub


class ConnectionBase:
    def __init__(self, local_key: PrivateKey, remote_key_pub: PublicKey, websocket: WebSocketCommonProtocol):
        self.local_key = local_key
        self.remote_key_pub = remote_key_pub
        self.device_id = get_device_id_from_key_pub(self.remote_key_pub)
        self.websocket = websocket

    @staticmethod
    def _check_message_for_errors(message):
        if message[:7] == 'ERROR: ':
            raise HubConnectionError(message[7:])
        return message

    async def send_raw(self, data):
        await self.websocket.send(data)

    async def send_encrypted(self, text):
        await self.send_raw(encrypt(text, self.remote_key_pub))

    async def get_raw(self):
        received_message = await self.websocket.recv()
        return self._check_message_for_errors(received_message)

    async def get_decrypted(self):
        return decrypt(await self.get_raw(), self.local_key)

    async def get(self):
        raise NotImplementedError()

    def __aiter__(self):
        return self

    async def __anext__(self):
        return await self.get()


class HubConnection(ConnectionBase):
    async def send(self, message: MessageToHub):
        await self.send_raw(str(message))

    async def get(self):
        return MessageFromHub.load(await self.get_raw())


class ClientConnection(ConnectionBase):
    async def send(self, message: MessageFromHub):
        await self.send_raw(str(message))

    async def get(self):
        return MessageToHub.load(await self.get_raw())
