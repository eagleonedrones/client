import random
from rsa import PublicKey, PrivateKey
from websockets import WebSocketCommonProtocol

from client.connection import ClientConnection, HubConnection
from client.errors import HubConnectionError
from utils.crypting import export_key_pub, import_key_pub


class HubMock(object):
    def __init__(self, key_pub: PublicKey, key: PrivateKey):
        self.key_pub = key_pub
        self.key = key


async def _raise_connection_error(error_message, websocket: WebSocketCommonProtocol):
    websocket.send(f'ERROR: { error_message }')
    await websocket.close(code=1003, reason=error_message)
    raise HubConnectionError(error_message)


async def hub_side_handshake(hub: HubMock, websocket: WebSocketCommonProtocol) -> ClientConnection:
    # send and get public keys
    await websocket.send(export_key_pub(hub.key_pub))
    client_pub_key = await websocket.recv()

    # validate string as client's public key
    try:
        connection = ClientConnection(hub.key, import_key_pub(client_pub_key), websocket)
    except ValueError:
        await _raise_connection_error('client has provided wrong or unsupported public key', websocket)
        raise HubConnectionError

    # wait to ask for question
    question = await connection.get_decrypted()
    if question != 'What\'s the question?':
        await _raise_connection_error('client asked wrong question', websocket)

    # send encrypted secret and wait for correct response
    # - all messages are encrypted since now
    generated_secret = 'secret-{}'.format(random.random())
    await connection.send_encrypted(generated_secret)
    answer = await connection.get_decrypted()
    if answer != generated_secret:
        await _raise_connection_error('client replied with wrong secret', websocket)

    # welcome and wait for response
    await connection.send_encrypted('Welcome!')
    response = await connection.get_decrypted()
    if response != 'Hi!':
        await _raise_connection_error('client replied with wrong welcome message', websocket)

    return connection


async def client_side_handshake(key_pub: PublicKey, remote_client: HubConnection):
    # send and get public keys
    await remote_client.send_raw(export_key_pub(key_pub))
    remote_client_key_pub = await remote_client.get_raw()

    # validate remote's key_pub as we already know it
    try:
        assert remote_client.remote_key_pub == import_key_pub(remote_client_key_pub)
    except (ValueError, AttributeError):
        await _raise_connection_error('unable to load HUB\'s public key', remote_client.websocket)
    except AssertionError:
        await _raise_connection_error('HUB presented wrong public key', remote_client.websocket)

    # get question and return unencrypted
    # - all messages are encrypted since now
    await remote_client.send_encrypted('What\'s the question?')
    secret = await remote_client.get_decrypted()
    await remote_client.send_encrypted(secret)

    # wait for welcome message
    welcome = await remote_client.get_decrypted()
    if welcome != 'Welcome!':
        await _raise_connection_error('HUB responded with wrong welcome message', remote_client.websocket)

    # finish handshake by respond
    await remote_client.send_encrypted('Hi!')
