from setuptools import setup, find_packages
setup(
    name="client",
    version="0.1.18",
    packages=find_packages(),
    # TODO: link these dependencies
    # install_requires=[
    #     'rsa>=4.0',
    #     'setuptools>=40.6.3',
    #     'sh>=1.12.14',
    #     'websockets>=7.0',
    #     'utils==0.1.5',
    # ],
    # dependency_links=[
    #     'git+ssh://git@bitbucket.org:/droneeagleone/utils.git#egg=utils-0.1.5'
    # ]
)
